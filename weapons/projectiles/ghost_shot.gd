extends Node2D

var direction : Vector2

var damage := 10
var type := ""
var fly_speed := 50.0
var fly_duration := 5.0
var knockback_power := 300.0

@onready var sprite_outer = $SpriteOuter

var source := "player"

var tilemap

# Called when the node enters the scene tree for the first time.
func _ready():
	for child in get_tree().current_scene.get_children():
		if child.has_method("exchange_tiles"):
			tilemap = child

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if fly_duration <= 0.0:
		delete_shot()
	
	position += direction * fly_speed * delta
	
	fly_duration -= delta
	
	check_for_tree()

func set_direction(dir):
	direction = dir

func _on_hit_box_body_entered(body):
	if body.is_in_group("terrain"):
		check_for_tree()

func setup(damage_input : int, speed_input : float, duration_input : float, \
			knockback_power_input : float, source_input : String, source_char):
	damage = damage_input if damage_input > 0 else damage
	
	fly_speed = speed_input if speed_input > 0 else fly_speed
	fly_duration = duration_input if duration_input > 0 else fly_duration
	knockback_power = knockback_power_input if knockback_power_input > 0 else knockback_power
	source = source_input if source_input != "" else source

func delete_shot():
	for child in get_tree().current_scene.get_children():
		if child.has_method("play_thud"):
			child.play_thud()
	
	queue_free()

func check_for_tree():
	var tile_pos = tilemap.local_to_map(global_position)
	var tile = tilemap.get_cell_tile_data(2, tile_pos)
	if tile != null:
		if tile.get_custom_data("isObstacle"):
			delete_shot()

func _on_hit_box_area_entered(area):
	if area.is_in_group("wall"):
		delete_shot()
