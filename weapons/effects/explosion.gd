extends Area2D

var damage := 100
var type := ""
var knockback_power := 0.0

var lifetime := 3
var hit_enemies := true

func _ready():
	for child in get_tree().current_scene.get_children():
		if child.has_method("play_explosion"):
			child.play_explosion()

func _on_tick_timer_timeout():
	lifetime -= 1
	
	if hit_enemies:
		for body in get_overlapping_bodies():
			if body.is_in_group("enemy"):
				body.hit(self)
		hit_enemies = false
	
	for child in get_tree().current_scene.get_children():
		if child.has_method("add_trauma"):
			child.add_trauma(1.0)
	
	if lifetime <= 0:
		queue_free()
