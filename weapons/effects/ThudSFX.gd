extends AudioStreamPlayer

const THUD = preload("res://assets/audio/sfx/momohut/thud.wav")
const THUD_1 = preload("res://assets/audio/sfx/momohut/thud1.wav")
const THUD_2 = preload("res://assets/audio/sfx/momohut/thud2.wav")

func play_thud():
	var selection = randi_range(1, 3)
	match selection:
		1:
			set_stream(THUD)
		2:
			set_stream(THUD_1)
		3:
			set_stream(THUD_2)
	
	play()
