extends Area2D

var damage := 5
var type := ""
var knockback_power := 0.0

var lifetime := 10

const SPLAT_00 = preload("res://assets/kenney-splat-pack/PNG/Default (256px)/splat00.png")
const SPLAT_01 = preload("res://assets/kenney-splat-pack/PNG/Default (256px)/splat01.png")
const SPLAT_02 = preload("res://assets/kenney-splat-pack/PNG/Default (256px)/splat02.png")
const SPLAT_03 = preload("res://assets/kenney-splat-pack/PNG/Default (256px)/splat03.png")
const SPLAT_04 = preload("res://assets/kenney-splat-pack/PNG/Default (256px)/splat04.png")
const SPLAT_05 = preload("res://assets/kenney-splat-pack/PNG/Default (256px)/splat05.png")
const SPLAT_06 = preload("res://assets/kenney-splat-pack/PNG/Default (256px)/splat06.png")
const SPLAT_07 = preload("res://assets/kenney-splat-pack/PNG/Default (256px)/splat07.png")
const SPLAT_08 = preload("res://assets/kenney-splat-pack/PNG/Default (256px)/splat08.png")
const SPLAT_09 = preload("res://assets/kenney-splat-pack/PNG/Default (256px)/splat09.png")
const SPLAT_10 = preload("res://assets/kenney-splat-pack/PNG/Default (256px)/splat10.png")
const SPLAT_11 = preload("res://assets/kenney-splat-pack/PNG/Default (256px)/splat11.png")
const SPLAT_12 = preload("res://assets/kenney-splat-pack/PNG/Default (256px)/splat12.png")
const SPLAT_13 = preload("res://assets/kenney-splat-pack/PNG/Default (256px)/splat13.png")
const SPLAT_14 = preload("res://assets/kenney-splat-pack/PNG/Default (256px)/splat14.png")
const SPLAT_15 = preload("res://assets/kenney-splat-pack/PNG/Default (256px)/splat15.png")
const SPLAT_16 = preload("res://assets/kenney-splat-pack/PNG/Default (256px)/splat16.png")
@onready var sprite_2d = $Sprite2D

var splats = [SPLAT_00, SPLAT_01, SPLAT_02, SPLAT_03, SPLAT_04, SPLAT_05,\
SPLAT_06, SPLAT_07, SPLAT_08, SPLAT_09, SPLAT_10, SPLAT_11, SPLAT_12,\
SPLAT_13, SPLAT_14, SPLAT_15, SPLAT_16]

@onready var audio_stream_player = $AudioStreamPlayer
const TSISH_1 = preload("res://assets/audio/sfx/momohut/tsish1.wav")
const TSISH_2 = preload("res://assets/audio/sfx/momohut/tsish2.wav")
const TSISH_3 = preload("res://assets/audio/sfx/momohut/tsish3.wav")
const PLOP = preload("res://assets/audio/sfx/momohut/plop.wav")

func _ready():
	sprite_2d.set_texture(splats[randi_range(0, 16)])
	audio_stream_player.set_stream(PLOP)
	audio_stream_player.play()

func _on_tick_timer_timeout():
	lifetime -= 1
	
	for body in get_overlapping_bodies():
		if body.is_in_group("enemy"):
			body.hit(self)
			play_tsish()
	
	if lifetime <= 0:
		queue_free()

func play_tsish():
	match randi_range(1, 3):
		1:
			audio_stream_player.set_stream(TSISH_1)
		2:
			audio_stream_player.set_stream(TSISH_2)
		2:
			audio_stream_player.set_stream(TSISH_2)
	audio_stream_player.play()
