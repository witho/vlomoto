extends Node2D

var spawn_chance := 40.0

@onready var spawners = $Spawners
@onready var music = $Music
@onready var enemy_shot_sfx = $EnemyShotSFX
@onready var chomp_sfx = $ChompSFX

var living_enemies := 0

# Called when the node enters the scene tree for the first time.
func _ready():
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func _on_spawn_timer_timeout():
	var spawns = spawners.get_children()
	
	var random_pick = spawns[randi_range(0, spawns.size() - 1)]
	
	for n in int(spawn_chance / 100.0):
		if living_enemies <= 60:
			random_pick.spawn_random_enemy()
			increment_enemies_count()
	
	var spawn_another = randi_range(1, 100)
	#print(str(spawn_another))
	if (spawn_another < int(spawn_chance) % 100) and living_enemies <= 60:
		random_pick.spawn_random_enemy()
		increment_enemies_count()
	
	spawn_chance += 0.5

func _on_music_finished():
	music.play()

func play_enemy_shot_sfx():
	enemy_shot_sfx.play()

func play_chomp_sfx():
	chomp_sfx.play()

func increment_enemies_count():
	living_enemies += 1

func decrement_enemies_count():
	living_enemies -= 1
