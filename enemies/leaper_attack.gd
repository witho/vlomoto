extends EnemyState

var target_direction
var initial_direction

var attack_finished := false

func enter(_msg := {}) -> void:
	enemy.trigger_attack = false
	attack_anim()
	#print("enemy attack")
	pass

func exit() -> void:
	pass

func physics_update(_delta) -> void:
	if not enemy.attack_timer.is_stopped():
		enemy.move_and_slide()
	
	if enemy.is_dying:
		state_machine.transition_to("Die")
	if attack_finished and not enemy.cancel_attack:
		state_machine.transition_to("Attack")
		attack_finished = false
	if attack_finished and enemy.cancel_attack:
		state_machine.transition_to("Chase")
		attack_finished = false

func attack_anim():
	var anim_tween = get_tree().create_tween()
	initial_direction = (enemy.player_reference.global_position - enemy.global_position)
	anim_tween.tween_property(enemy.sprite_2d, "scale", Vector2(0.5, 0.5), 0.5).\
	set_trans(Tween.TRANS_LINEAR).set_ease(Tween.EASE_OUT)
	anim_tween.tween_property(enemy.sprite_2d, "scale", Vector2(1, 1), 0.2).\
	set_trans(Tween.TRANS_LINEAR).set_ease(Tween.EASE_OUT)
	await anim_tween.finished
	target_direction = (enemy.player_reference.global_position - enemy.global_position)
	target_direction = (target_direction + initial_direction) / 2.0 # make enemy not 100% accurate
	enemy.velocity = target_direction.normalized() * enemy.jump_power
	enemy.attack_timer.start()
	enemy.direction = target_direction.normalized()

func _on_attack_timer_timeout():
	attack_finished = true
