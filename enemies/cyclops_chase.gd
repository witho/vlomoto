extends EnemyState

var target_direction

var calculated_target
var initial_direction

var attack_finished := false
var attack_spread := 10.0
var runes : Array[String]

const GHOST_SHOT = preload("res://weapons/projectiles/ghost_shot.tscn")

func enter(_msg := {}) -> void:
	enemy.trigger_navigation.start()
	calculate_direction()
	attack_anim()
	#print("enemy chase")
	enemy.animation_player.play("chase")
	pass

func exit() -> void:
	enemy.trigger_navigation.stop()
	enemy.animation_player.stop()
	pass

func physics_update(delta) -> void:
	# var target_direction = (enemy.player_reference.global_position - enemy.global_position).normalized()
	enemy.navigation_agent_2d.set_velocity(lerp(enemy.velocity, target_direction * enemy.speed, enemy.acceleration * delta))
	
	if enemy.is_dying:
		state_machine.transition_to("Die")
	if not enemy.detection_range.get_overlapping_bodies().has(enemy.player_reference):
		state_machine.transition_to("Patrol")

func _on_trigger_navigation_timeout():
	calculate_direction()

func calculate_direction():
	enemy.navigation_agent_2d.target_position = enemy.player_reference.global_position
	target_direction = (enemy.navigation_agent_2d.get_next_path_position() - enemy.global_position).normalized()

func attack_anim():
	if not enemy.attack_range.get_overlapping_bodies().has(enemy.player_reference):
		enemy.attack_timer.start()
		return
	
	initial_direction = (enemy.player_reference.global_position - enemy.global_position)
	
	calculated_target = calculate_target(enemy.player_reference.global_position - enemy.global_position, attack_spread)
	var bullet = GHOST_SHOT.instantiate()
	# set bullet damage, type, speed and duration (0 or "" for defaults)
	bullet.setup(10, 0, 0, 0, "enemy", self)
	bullet.set_global_position(enemy.global_position)
	bullet.set_direction(calculated_target)
	bullet.look_at(enemy.global_position + calculated_target)
	bullet.global_position += calculated_target.normalized() * 20
	get_tree().current_scene.add_child(bullet)
	
	enemy.attack_timer.start()
	
	if get_tree().current_scene.has_method("play_enemy_shot_sfx"):
		get_tree().current_scene.play_enemy_shot_sfx()

func _on_attack_timer_timeout():
	attack_finished = true
	attack_anim()

# add randomized spread to attack
func calculate_target(target : Vector2, spread : float):
	var perpendicular = Vector2(target.y, -target.x).normalized()
	return (target + (randf_range(-1.0, 1.0) * perpendicular * spread)).normalized()

func _on_navigation_agent_2d_velocity_computed(safe_velocity):
	if state_machine.state == self:
		enemy.velocity = safe_velocity
		enemy.move_and_slide()
