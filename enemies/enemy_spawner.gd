extends Marker2D

const GHOST = preload("res://enemies/ghost.tscn")
const SPIDER = preload("res://enemies/spider.tscn")
const SUPER_GHOST = preload("res://enemies/super_ghost.tscn")
const CYCLOPS = preload("res://enemies/cyclops.tscn")

@onready var no_spawn_zone = $NoSpawnZone

func spawn_random_enemy():
	if player_overlaps():
		return
	
	var spawn
	
	match randi_range(1, 10):
		1, 2, 3, 4:
			spawn = GHOST.instantiate()
		5, 6, 7:
			spawn = SPIDER.instantiate()
		8, 9:
			spawn = SUPER_GHOST.instantiate()
		10:
			spawn = CYCLOPS.instantiate()
	
	spawn.set_global_position(global_position)
	get_tree().current_scene.add_child(spawn)

func spawn_specific_enemy(input_string : String):
	if player_overlaps():
		return
	
	var spawn
	
	match input_string:
		"ghost":
			spawn = GHOST.instantiate()
		"spider":
			spawn = SPIDER.instantiate()
		"super_ghost":
			spawn = SUPER_GHOST.instantiate()
		"cyclops":
			spawn = CYCLOPS.instantiate()
	
	spawn.set_global_position(global_position)
	get_tree().current_scene.add_child(spawn)

func player_overlaps():
	var overlaps = no_spawn_zone.get_overlapping_bodies()
	for body in overlaps:
		if body.is_in_group("player"):
			return true
	return false
