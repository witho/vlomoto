class_name Enemy
extends CharacterBody2D

@export_category("movement")
@export var speed := 75
@export var acceleration := 18
@export var friction := 13
@export var jump_power := 300

@export_category("stats")
@export var health := 50

var floating_text = preload("res://UI/floating_text.tscn")

var player_detected := false
var player_reference : Player
var trigger_attack := false
var cancel_attack := false
var is_dying := false

var drop_chance := 0.3

const AMMO_PICKUP = preload("res://objects/ammo_pickup.tscn")
const FOOD_PICKUP = preload("res://objects/food_pickup.tscn")
const AMMO_PICKUP_SWISS = preload("res://objects/ammo_pickup_swiss.tscn")
const AMMO_PICKUP_NACHO = preload("res://objects/ammo_pickup_nacho.tscn")
const AMMO_PICKUP_AMERICAN = preload("res://objects/ammo_pickup_american.tscn")

func knockback(direction, knockback_power):
	velocity = velocity / 2
	velocity += direction * knockback_power

func flash():
	var flash_tween = get_tree().create_tween()
	flash_tween.tween_property($Sprite2D, "modulate", Color(5, 5, 5, 1), 0.1)
	flash_tween.tween_property($Sprite2D, "modulate", Color(1, 1, 1, 1), 0.1)

func spawn_pickup():
	if randf() > drop_chance:
		return
	
	var random_choice = randi_range(1, 109)
	#print("random choice " + str(random_choice))
	
	if range(101,110).has(random_choice):
		spawn_food(random_choice - 100)
	else:
		spawn_ammo(random_choice)
	

func spawn_food(_random_choice):
	#print("spawn food " + str(random_choice))
	
	var new_food = FOOD_PICKUP.instantiate()
	
	new_food.pizza_count = player_reference.pizza_parts.values().count(true)
	
	new_food.set_global_position(global_position)
	get_tree().current_scene.add_child(new_food)

func spawn_ammo(random_choice):
	#print("spawn ammo " + str(random_choice))
	
	var new_ammo
	
	if range(1,44).has(random_choice):
		new_ammo = AMMO_PICKUP_SWISS.instantiate()
	elif range(45,80).has(random_choice):
		new_ammo = AMMO_PICKUP_NACHO.instantiate()
	elif range(81,100).has(random_choice):
		new_ammo = AMMO_PICKUP_AMERICAN.instantiate()
	
	if new_ammo == null:
		return
	
	new_ammo.set_global_position(global_position)
	get_tree().current_scene.add_child(new_ammo)
