extends EnemyState

func enter(_msg := {}) -> void:
	death_anim()

func physics_update(_delta) -> void:
	pass

func death_anim() -> void:
	for child in get_tree().current_scene.get_children():
		if child.has_method("increment_kills"):
			child.increment_kills()
	
	if get_tree().current_scene.has_method("decrement_enemies_count"):
		get_tree().current_scene.decrement_enemies_count()
	
	var anim_tween = get_tree().create_tween()
	anim_tween.tween_property(enemy, "modulate", Color(1, 1, 1, 0), 1.0)
	
	await enemy.spawn_pickup()
	enemy.queue_free()
