extends EnemyState

func enter(_msg := {}) -> void:
	#print("enemy patrol")
	pass

func physics_update(delta) -> void:
	enemy.velocity = lerp(enemy.velocity, Vector2.ZERO, enemy.friction * delta)
	enemy.move_and_slide()
	
	if enemy.is_dying:
		state_machine.transition_to("Die")
	if enemy.detection_range.get_overlapping_bodies().has(enemy.player_reference):
		state_machine.transition_to("Chase")
