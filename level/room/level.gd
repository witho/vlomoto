extends Node

const base = "res://vladi-levels/"
@onready var bolder = load("res://objects/bolder.tscn")

func createInstance(root, rooms, weight, attach: Marker2D, count, depth):
	print("createInstance(%d, %d)" % [count, depth])
	var inst: TileMap = rooms[selectRoom(rooms, weight)]["resource"].instantiate()
	var tilemap: TileMap = inst
	var exits = inst.find_child("Exits").get_children()
	var x = 0
	var y = 0
	var found = false
	for exit: Marker2D in exits:
		if attach == null or is_equal_approx(absf(exit.rotation_degrees - attach.rotation_degrees), 180):
			root.add_child(inst)
			var offset: Vector2 = Vector2()
			if attach != null:
				offset = attach.global_position - exit.global_position
			print("Found matching exits: " + str(exit.rotation_degrees) + " and " + str(attach.rotation_degrees if attach else attach))
			print("    " + str(exit.global_position) + " and " + str(attach.global_position if attach else attach) + " -> " + str(offset))
			inst.position += offset
			if attach == null or no_overlap(root, inst):
				count = count - 1
				found = true
				for next in exits:
					if next != exit:
						var oldcount = count
						if count > 0 and depth > 0:
							count = createInstance(root, rooms, weight, next, count, depth - 1)
						if oldcount == count:
							var b: Node2D = bolder.instantiate()
							inst.add_child(b)
							b.global_position = next.global_position
			else:
				inst.queue_free()
	if not found:
		print("No matching exit found")
		inst.queue_free()
	return count

func no_overlap(root: Node, inst: TileMap):
	var gr = global_used_rect(inst)
	for other in root.get_children():
		var other_rect = global_used_rect(other)
		if inst != other:
			print("Check %s overlaps %s?" % [gr, other_rect])
			if gr.intersects(other_rect):
				print("Overlapping")
				return false
	print("Not overlapping anything")
	return true

func global_used_rect(tilemap: TileMap):
	var rect = tilemap.get_used_rect()
	rect.position = Vector2i(tilemap.to_global(rect.position))
	rect.size *= tilemap.tile_set.tile_size.x
	return rect

func selectRoom(rooms, weight):
	var r = randi_range(0, weight - 1)
	for room in rooms:
		if (r < rooms[room]["weight"]):
			print("Using room " + room)
			return room
		r -= rooms[room]["weight"]
	print("No room found")
	return null

func load(filename, root):
	var f = FileAccess.open(filename, FileAccess.READ)
	var json = JSON.parse_string(f.get_as_text())
	var rooms = json["rooms"]
	var weight = 0
	var count = json["count"]
	var depth = json["depth"]
	for key in rooms:
		var file = base + key + ".tscn"
		rooms[key]["resource"] = load(file)
		weight += rooms[key]["weight"]

	createInstance(root, rooms, weight, null, count, depth)
