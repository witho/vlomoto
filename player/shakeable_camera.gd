extends Camera2D

@onready var player : Player

@export var camera_offset := 40
@export var trauma_reduction_rate := 1.0
@export var noise : FastNoiseLite
@export var noise_speed := 50.0

@export var max_x := 50.0
@export var max_y := 50.0

var trauma := 0.0

var time := 0.0

func _ready():
	var siblings = get_parent().get_children()
	for sib in siblings:
		if sib.is_in_group("player"):
			player = sib

func _process(delta):
	time += delta
	trauma = max(trauma - delta * trauma_reduction_rate, 0.0)
	
	offset.x = max_x * get_shake_intensity() * get_noise_from_seed(0)
	offset.y = max_y * get_shake_intensity() * get_noise_from_seed(1)
	
	var direction = (get_global_mouse_position() - player.global_position).normalized()
	
	global_position = player.global_position + (direction * camera_offset)
	pass

func add_trauma(trauma_amount : float):
	trauma = clamp(trauma + trauma_amount, 0.0, 1.0)

func get_shake_intensity() -> float:
	return trauma * trauma

func get_noise_from_seed(_seed : int) -> float:
	noise.seed = _seed
	return noise.get_noise_1d(time * noise_speed)
