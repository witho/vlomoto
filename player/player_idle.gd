extends PlayerState

#@export var _animation_player : NodePath
#@onready var animation_player:AnimationPlayer = get_node(_animation_player)

func enter(_msg := {}) -> void:
	#animation_player.play("Idle")
	pass

func physics_update(delta) -> void:
	#player.sprite_2d.scale.y = lerp(player.sprite_2d.scale.y, 1.0, 1 - pow(0.1, delta))
	#player.sprite_2d.scale.x = lerp(player.sprite_2d.scale.x, 1.0, 1 - pow(0.1, delta))
	
	player.velocity = lerp(player.velocity, Vector2.ZERO, player.friction * delta)
	player.move_and_slide()
	
	player.pivot_weapon()
	
	if player.is_dying:
		state_machine.transition_to("Die")
	elif not player.get_input_direction().is_zero_approx():
		state_machine.transition_to("Run")
	elif Input.is_action_pressed("attack") and player.can_attack():
		player.attack()
