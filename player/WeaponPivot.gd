extends Node2D

const PISTOL = preload("res://weapons/pistol.tscn")
const STEAM_PISTOL = preload("res://weapons/steam_pistol.tscn")

func change_weapon(weapon_name : String):
	match weapon_name:
		"SteamPistol":
			get_child(0).queue_free()
			var new = STEAM_PISTOL.instantiate()
			add_child(new)
