extends Marker2D

func spawn_player(player : Player, camera : Camera2D):
	camera.set_position_smoothing_enabled(false)
	player.set_global_position(global_position)
	await get_tree().create_timer(0.2).timeout
	camera.set_position_smoothing_enabled(true)
