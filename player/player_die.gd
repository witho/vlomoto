extends PlayerState

const GAME_OVER = preload("res://UI/game_over.tscn")

func enter(_msg := {}) -> void:
	death_anim()

func physics_update(delta) -> void:
	player.velocity = lerp(player.velocity, Vector2.ZERO, player.friction / 2.0 * delta)

func death_anim() -> void:
	var game_over_ui = GAME_OVER.instantiate()
	
	get_tree().current_scene.add_child(game_over_ui)
	
	game_over_ui.setup(player.shots_fired, player.shots_hit, player.kills, player.ammo)
	
	
	player.animation_player.play("death")
