extends PlayerState

#@export var _animation_player : NodePath
#@onready var animation_player:AnimationPlayer = get_node(_animation_player)

var dodge_timer : float = 0.0
var dodge_cooldown : float = 0.0
var dodge_direction : Vector2

const SWISH = preload("res://assets/audio/sfx/momohut/swish.wav")

func enter(_msg := {}) -> void:
	dodge_timer = 0.0
	player.count_dodge()
	
	dodge_direction = player.get_input_direction()
	
	player.dodge_sfx.set_stream(SWISH)
	player.dodge_sfx.play()
	
	# set invulnerability (be sure to reset in other states!)

func physics_update(delta) -> void:
	if player.is_dying:
		state_machine.transition_to("Die")
	elif player.dodge_time > dodge_timer:
		player.velocity = dodge_direction * player.dodge_velocity
	else:
		state_machine.transition_to("Run")
	
	#player.sprite_2d.scale.y = ease(remap(abs(player.velocity.y), 0, abs(player.jump_impulse), 0.75 * player.player_scale, 1.75 * player.player_scale), 1.0)
	#player.sprite_2d.scale.x = ease(remap(abs(player.velocity.y), 0, abs(player.jump_impulse), 1.25 * player.player_scale, 0.75 * player.player_scale), 1.0)
	
	dodge_timer += delta
	
	player.move_and_slide()
