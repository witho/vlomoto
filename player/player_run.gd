extends PlayerState

#@export var _animation_player : NodePath
#@onready var animation_player:AnimationPlayer = get_node(_animation_player)
const SWISH = preload("res://assets/audio/sfx/momohut/swish.wav")

func enter(_msg := {}) -> void:
	#animation_player.play("Run")
	player.animation_player.play("run")
	pass

func physics_update(delta) -> void:
	#player.sprite_2d.scale.y = lerp(player.sprite_2d.scale.y, 1.0, 1 - pow(0.1, delta))
	#player.sprite_2d.scale.x = lerp(player.sprite_2d.scale.x, 1.0, 1 - pow(0.1, delta))
	
	if not player.get_input_direction().is_zero_approx():
		player.velocity = lerp(player.velocity, player.get_input_direction() * player.speed, player.acceleration * delta)
	
	player.move_and_slide()
	
	player.pivot_weapon()
	
	if player.is_dying:
		state_machine.transition_to("Die")
	elif Input.is_action_just_pressed("dodge") and player.can_dodge():
		state_machine.transition_to("Dodge")
	elif player.get_input_direction().is_zero_approx():
		state_machine.transition_to("Idle")
	elif Input.is_action_pressed("attack") and player.can_attack():
		player.attack()

func exit():
	player.animation_player.stop()
