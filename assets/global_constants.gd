extends Node

var color_typeless = Color("cccccc")
var color_fire = Color("ff3131")
var color_water = Color("3131ff")

enum DAMAGE_TYPES {typeless, fire, water}
enum CHEESE_TYPE {mozarella, american, feta, nacho, camembert, swiss, cottage, pepperjack, pizza}
