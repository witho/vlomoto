extends TileMap

# Called when the node enters the scene tree for the first time.
func _ready():
	exchange_tiles()

func exchange_tiles():
	var obstacles = get_used_cells(2)
	for coords in obstacles:
		var cell = get_cell_tile_data(2, coords)
		if cell.get_collision_polygons_count(0) > 0:
			set_cell(1, coords, get_cell_source_id(0, coords), get_cell_atlas_coords(0, coords))
			set_cell(0, coords)
