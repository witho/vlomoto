extends Area2D

@export var type : GlobalConstants.DAMAGE_TYPES

func _on_body_entered(body):
	# TODO: Make rune disappear when added to player
	
	if body.is_in_group("player"):
		match type:
			GlobalConstants.DAMAGE_TYPES.fire:
				body.add_rune("fire")
			GlobalConstants.DAMAGE_TYPES.water:
				body.add_rune("water")
			_:
				pass
