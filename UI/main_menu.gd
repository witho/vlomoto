extends CanvasLayer

const RUIN_ARENA = preload("res://arena_levels/ruin_arena.tscn")

func _on_button_pressed():
	get_tree().change_scene_to_packed(RUIN_ARENA)
