extends CanvasLayer

@onready var kills = $MarginContainer/VBoxContainer/HBoxContainer/MarginContainer/MarginContainer/HBoxContainer/VBoxContainer2/MarginContainer4/Kills
@onready var shots = $MarginContainer/VBoxContainer/HBoxContainer/MarginContainer/MarginContainer/HBoxContainer/VBoxContainer2/MarginContainer2/Shots
@onready var hits = $MarginContainer/VBoxContainer/HBoxContainer/MarginContainer/MarginContainer/HBoxContainer/VBoxContainer2/MarginContainer3/Hits
@onready var accuracy = $MarginContainer/VBoxContainer/HBoxContainer/MarginContainer/MarginContainer/HBoxContainer/VBoxContainer2/MarginContainer6/Accuracy

@onready var mozarella = $MarginContainer/VBoxContainer/HBoxContainer/MarginContainer2/MarginContainer/VBoxContainer/HBoxContainer2/Mozarella
@onready var swiss = $MarginContainer/VBoxContainer/HBoxContainer/MarginContainer2/MarginContainer/VBoxContainer/HBoxContainer2/Swiss
@onready var nacho = $MarginContainer/VBoxContainer/HBoxContainer/MarginContainer2/MarginContainer/VBoxContainer/HBoxContainer2/Nacho
@onready var american = $MarginContainer/VBoxContainer/HBoxContainer/MarginContainer2/MarginContainer/VBoxContainer/HBoxContainer3/American
@onready var pizza = $MarginContainer/VBoxContainer/HBoxContainer/MarginContainer2/MarginContainer/VBoxContainer/HBoxContainer3/Pizza

# Called when the node enters the scene tree for the first time.
func _ready():
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	pass

func setup(shots_in, hits_in, kills_in, ammo):
	shots.set_text(str(shots_in))
	hits.set_text(str(hits_in))
	kills.set_text(str(kills_in))
	
	var acc := 0.0
	if shots_in > 0:
		acc = float(hits_in) / float(shots_in) * 100.0
	accuracy.set_text(str("%.2f" % acc) + "%")
	
	var show_modulate = Color(1.0, 1.0, 1.0, 1.0)
	for ammo_type in ammo:
		match ammo_type:
			GlobalConstants.CHEESE_TYPE.swiss:
				swiss.set_modulate(show_modulate)
			GlobalConstants.CHEESE_TYPE.nacho:
				nacho.set_modulate(show_modulate)
			GlobalConstants.CHEESE_TYPE.american:
				american.set_modulate(show_modulate)
			GlobalConstants.CHEESE_TYPE.pizza:
				pizza.set_modulate(show_modulate)

func _on_new_game_button_pressed():
	get_tree().reload_current_scene()
