extends Marker2D

@onready var label = $Label

var value = 0
var call_done = Callable(self, "tween_complete")
var type = ""

var velocity = Vector2.ZERO

func _ready():
	label.set_text(str(value))
	
	set_color()
	
	randomize()
	var side_movement = randi() % 81 - 40
	velocity = Vector2(side_movement, 25)
	
	var scale_tween = get_tree().create_tween()
	scale_tween.tween_property(self, "scale", Vector2(1, 1), 0.2).\
	set_trans(Tween.TRANS_LINEAR).set_ease(Tween.EASE_OUT)
	scale_tween.tween_property(self, "scale", Vector2(0.1, 0.1), 0.7).\
	set_trans(Tween.TRANS_LINEAR).set_ease(Tween.EASE_OUT).set_delay(0.3)
	
	scale_tween.tween_callback(call_done)

func _process(delta):
	position -= velocity * delta

func tween_complete():
	queue_free()

func set_color():
	match type:
		"fire":
			label.set("theme_override_colors/font_color", GlobalConstants.color_fire)
		"water":
			label.set("theme_override_colors/font_color", GlobalConstants.color_water)
		_:
			label.set("theme_override_colors/font_color", GlobalConstants.color_typeless)
